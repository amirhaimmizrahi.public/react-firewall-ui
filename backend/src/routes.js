var express = require("express");
var router = express.Router();

const { loginHandler, signupHandler, fetchHandler, addHandler, modifyHandler, removeHandler } = require("./handlers");

router.post("/login", async function (req, res, next) {
  const i = await loginHandler(req.body);
  console.log(i)
  res.send(i);
});

router.post("/signup", async function (req, res, next) {
  res.send(await signupHandler(req.body));
});

router.post("/fetch", async function (req, res, next) {
  res.json(await fetchHandler(req.body));
});

router.post("/add", async function (req, res, next) {
  res.send(await addHandler(req.body));
});

router.post("/modify", async function (req, res, next) {
  res.send(await modifyHandler(req.body));
});

router.post("/remove", async function (req, res, next) {
  res.send(await removeHandler(req.body));
});

module.exports = router;
