const { User, UserInfo } = require("../schemas/user");
const { Rule } = require("../schemas/rule");
const validateIP = require("validate-ip-node");

function validatePort(port) {
  return port.match("^[0-9]+$") && port >= 0 && port <= 65536;
}

async function userValidation(reqBody) {
  const { username, password } = reqBody;
  const user = await User.findOne({ username });

  if (!(username && password)) {
    return "please provide username and password";
  }
  if (!user || user.password != password) {
    return "please make sure that your username and password are corrent";
  }
  return "ok";
}

async function loginHandler(reqBody) {
  return await userValidation(reqBody);
}

async function signupHandler(reqBody) {
  const { username, password, passwordConfirm } = reqBody;

  if (!(username && password && passwordConfirm)) {
    return "please provide username, password and password confirm";
  }
  if (password != passwordConfirm) {
    return "password does not match the password confirm";
  }
  if (await User.exists({ username })) {
    return "username is taken";
  }
  const newUser = new User({
    username,
    password,
    inbound: [],
    outbound: [],
  });

  await newUser.save();
  return "ok";
}

async function fetchHandler(reqBody) {
  const result = await userValidation(reqBody);
  if (result != "ok") return result;

  const { username } = reqBody;
  const user = (
    await User.findOne({ username }).populate("inbound").populate("outbound")
  ).toJSON();

  return { inbound: user.inbound, outbound: user.outbound };
}

async function addHandler(reqBody) {
  const result = await userValidation(reqBody);
  if (result != "ok") return result;

  const { username, rule } = reqBody;

  if (
    !(
      rule.name &&
      rule.srcIp &&
      rule.destIp &&
      rule.srcPort &&
      rule.destPort &&
      rule.action &&
      rule.isInbound !== undefined
    )
  ) {
    return "please fill all the fields";
  }

  if (!(validateIP(rule.srcIp) && validateIP(rule.destIp))) {
    return "invalid source or destination ip";
  }

  if (!(validatePort(rule.srcPort) && validatePort(rule.destPort))) {
    return "invalid source or destination port";
  }

  const user = await User.findOne({ username })
    .populate("inbound")
    .populate("outbound");
  const newRule = new Rule({
    name: rule.name,
    srcIp: rule.srcIp,
    destIp: rule.destIp,
    srcPort: Number(rule.srcPort),
    destPort: Number(rule.destPort),
    action: rule.action,
    isInbound: rule.isInbound,
  });

  if (rule.isInbound) {
    if (user.inbound.find((current) => current.name === rule.name)) {
      return "rule with the same name is already exists";
    }

    user.inbound.push(newRule._id);
  } else {
    if (user.outbound.find((current) => current.name === rule.name)) {
      return "rule with the same name is already exists";
    }

    user.outbound.push(newRule._id);
  }
  await newRule.save();
  await user.save();
  return "ok";
}

async function modifyHandler(reqBody) {
  const result = await userValidation(reqBody);
  if (result != "ok") return result;

  const { username, ruleName, rule } = reqBody;
  if (
    !(
      rule.name &&
      rule.srcIp &&
      rule.destIp &&
      rule.srcPort &&
      rule.destPort &&
      rule.action &&
      rule.isInbound !== undefined
    )
  ) {
    return "please fill all the fields";
  }

  if (!(validateIP(rule.srcIp) && validateIP(rule.destIp))) {
    return "invalid source or destination ip";
  }

  if (!(validatePort(rule.srcPort) && validatePort(rule.destPort))) {
    return "invalid source or destination port";
  }
  const user = await User.findOne({ username })
    .populate("inbound")
    .populate("outbound");

  const ruleToModify = await Rule.findOne({
    name: ruleName,
    isInbound: rule.isInbound,
  });
  let ruleWithSameName;
  if (rule.isInbound) {
    ruleWithSameName = user.inbound.find(
      (current) => current.name === rule.name && current.name !== ruleName
    );
  } else {
    ruleWithSameName = user.outbound.find(
      (current) => current.name === rule.name && current.name !== ruleName
    );
  }

  if (ruleWithSameName && ruleWithSameName._id != ruleToModify._id) {
    return "rule with the same name is already exists";
  }

  if (!ruleToModify) {
    return "rule was not found";
  }

  ruleToModify.name = rule.name;
  ruleToModify.action = rule.action;
  ruleToModify.srcIp = rule.srcIp;
  ruleToModify.destIp = rule.destIp;
  ruleToModify.srcPort = rule.srcPort;
  ruleToModify.destPort = rule.destPort;
  ruleToModify.isInbound = rule.isInbound;

  await ruleToModify.save();

  return "ok";
}

async function removeHandler(reqBody) {
  const result = await userValidation(reqBody);
  if (result != "ok") return result;

  const { username, ruleName, isInbound } = reqBody;

  if (!ruleName || isInbound === undefined) {
    return "please specify the rule to remove";
  }

  const user = await User.findOne({ username })
    .populate("inbound")
    .populate("outbound");

  if (isInbound) {
    const ruleToRemove = user.inbound.find((rule) => rule.name === ruleName);
    const index = user.inbound.indexOf(ruleToRemove);
    user.inbound.splice(index, 1);
  } else {
    const ruleToRemove = user.outbound.find((rule) => rule.name === ruleName);
    const index = user.outbound.indexOf(ruleToRemove);
    user.outbound.splice(index, 1);
  }
  await user.save();
  return "ok";
}

module.exports = {
  loginHandler,
  signupHandler,
  fetchHandler,
  addHandler,
  modifyHandler,
  removeHandler,
};
