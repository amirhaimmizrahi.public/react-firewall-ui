const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  inbound: [{
    type: Schema.Types.ObjectId,
    ref: "Rule",
    default: [],
  }],
  outbound: [{
    type: Schema.Types.ObjectId,
    ref: "Rule",
    default: [],
  }],
});

userSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret, options) => {
      delete ret.__v;
      delete ret._id;
      ret.inbound.forEach(rule => {
        delete rule.__v;
        delete rule._id;
      });
  },
});

const User = mongoose.model("User", userSchema);

module.exports = { User };
