const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ruleSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  srcIp: {
    type: String,
    required: true,
  },
  destIp: {
    type: String,
    required: true,
  },
  srcPort: {
    type: Number,
    required: true,
  },
  destPort: {
    type: Number,
    required: true,
  },
  action: {
    type: String,
    required: true,
  },
  isInbound: {
    type: Boolean,
    required: true
  }
});

const Rule = mongoose.model("Rule", ruleSchema);

module.exports = { Rule };
