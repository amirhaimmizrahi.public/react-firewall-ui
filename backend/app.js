var express = require("express");
var app = express();
var mongoose = require("mongoose");
var cors = require("cors");

var indexRouter = require("./src/routes");
var port = process.env.PORT || 9001;

app.use(cors());
app.use(express.json());
app.use("/", indexRouter);

mongoose
  .connect("mongodb://localhost:27017/firewall", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((result) => console.log("database connected"))
  .catch((err) => console.log(err));

app.listen(port, () => {
  console.log("server started on port " + port);
});