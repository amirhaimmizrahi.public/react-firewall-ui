import { Grid, Typography } from "@mui/material";
import React from "react";

export default function RulesHeader() {
  return (
    <Grid
      container
      direction="row"
      sx={{ width: "50%", height: "40px", margin: "5px" }}
    >
      <Grid item flex={1} align="center">
        <Typography>Name</Typography>
      </Grid>
      <Grid item flex={1} align="center">
        <Typography>Source IP</Typography>
      </Grid>
      <Grid item flex={1} align="center">
        <Typography>Destination IP</Typography>
      </Grid>
      <Grid item flex={1} align="center">
        <Typography>Source Port</Typography>
      </Grid>
      <Grid item flex={1} align="center">
        <Typography>Destination port</Typography>
      </Grid>
      <Grid item flex={1} align="center">
        <Typography>action</Typography>
      </Grid>
    </Grid>
  );
}
