import { Alert, Button, Grid, Paper, TextField } from "@mui/material";
import React, { useRef, useState } from "react";
import { useModify } from "../../api/handlers";

export default function ModifyRule({
  name,
  srcIp,
  destIp,
  srcPort,
  destPort,
  action,
  isInbound,
}) {
  const ruleNameRef = useRef();
  const ruleSrcIpRef = useRef();
  const ruleDestIpRef = useRef();
  const ruleSrcPortRef = useRef();
  const ruleDestPortRef = useRef();
  const ruleActionRef = useRef();
  const [modifyError, setModifyError] = useState("");
  const modify = useModify();

  return (
    <>
      {modifyError ? (
        <Alert sx={{ margin: "5px" }} severity="error">
          {modifyError}
        </Alert>
      ) : null}
      <Paper elevation={10} sx={{ padding: "0px", margin: "5px" }}>
        <Grid
          container
          width="100%"
          justifyContent="center"
          alignItems="center"
        >
          <TextField
            key={name + "n"}
            inputRef={ruleNameRef}
            defaultValue={name}
            label="Rule Name"
            placeholder="Enter the rule name"
            sx={{ margin: "10px" }}
          />
          <TextField
            key={srcIp + "si"}
            inputRef={ruleSrcIpRef}
            defaultValue={srcIp}
            label="Source IP"
            placeholder="Enter the source ip"
            sx={{ margin: "10px" }}
          />
          <TextField
            key={destIp + "di"}
            inputRef={ruleDestIpRef}
            defaultValue={destIp}
            label="Destination IP"
            placeholder="Enter the destination ip"
            sx={{ margin: "10px" }}
          />
          <TextField
            key={srcPort + "sp"}
            inputRef={ruleSrcPortRef}
            defaultValue={srcPort}
            label="Source Port"
            placeholder="Enter the source port"
            sx={{ margin: "10px" }}
          />
          <TextField
            key={destPort + "dp"}
            inputRef={ruleDestPortRef}
            defaultValue={destPort}
            label="Destination Port"
            placeholder="Enter the destination port"
            sx={{ margin: "10px" }}
          />
          <TextField
            key={action + "d"}
            inputRef={ruleActionRef}
            defaultValue={action}
            label="Rule action"
            placeholder="Enter the rule action"
            sx={{ margin: "10px" }}
            multiline
          />
          <Button
            sx={{ margin: "10px" }}
            onClick={async () => {
              const error = await modify(
                name,
                ruleNameRef.current.value,
                ruleActionRef.current.value,
                ruleSrcIpRef.current.value,
                ruleDestIpRef.current.value,
                ruleSrcPortRef.current.value,
                ruleDestPortRef.current.value,
                isInbound
              );
              setModifyError(error);
            }}
          >
            modify
          </Button>
        </Grid>
      </Paper>
    </>
  );
}
