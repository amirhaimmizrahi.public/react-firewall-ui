import { Button } from '@mui/material';
import { useNavigate } from "react-router-dom";
import React from 'react'
import { useDispatch } from 'react-redux';
import {clear} from '../../dl/slices/rules'

export default function LogoutButton() {
    const navigate = useNavigate()
    const dispatch = useDispatch()

    return (
        <Button
          variant="outlined"
          sx={{ position: "absolute", right: 10, top: 10 }}
          onClick={() => {
            navigate("../login");
            dispatch(clear());
          }}
        >
          Logout{" "}
        </Button>
      );
}