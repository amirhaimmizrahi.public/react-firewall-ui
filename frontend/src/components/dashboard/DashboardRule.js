import {
  Button,
  Collapse,
  Grid,
  IconButton,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import ModifyRule from "./ModifyRule";
import { useRemove } from "../../api/handlers";

export default function DashboardRule({
  name,
  srcIp,
  destIp,
  srcPort,
  destPort,
  action,
  isInbound,
  isModifying,
  setModifiedRule,
}) {
  const [isHover, setIsHover] = useState(false);
  const remove = useRemove();

  return (
    <>
      <Button
        sx={{
          width: "50%",
          height: "40px",
          margin: "5px",
          backgroundColor: "#333",
          textTransform: "none",
          borderRadius: "7px",
        }}
        onMouseEnter={() => {
          setIsHover(true);
        }}
        onMouseLeave={() => {
          setIsHover(false);
        }}
        onClick={() => {
          if (isModifying) {
            setModifiedRule("");
          } else {
            setModifiedRule(name);
          }
        }}
      >
        <Grid item flex={1} align="center">
          <Typography>{name}</Typography>
        </Grid>
        <Grid item flex={1} align="center">
          <Typography>{srcIp}</Typography>
        </Grid>
        <Grid item flex={1} align="center">
          <Typography>{destIp}</Typography>
        </Grid>
        <Grid item flex={1} align="center">
          <Typography>{srcPort}</Typography>
        </Grid>
        <Grid item flex={1} align="center">
          <Typography>{destPort}</Typography>
        </Grid>
        <Grid item flex={1} align="center">
          <Typography>{action}</Typography>
        </Grid>
        {isHover ? (
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            flex={0.25}
            sx={{
              position: "absolute",
              left: "calc(50% - 50% / 16)",
            }}
          >
            <Tooltip title="delete">
              <IconButton
                color="primary"
                onClick={() => {
                  remove(name, isInbound);
                }}
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        ) : null}
      </Button>
      <Collapse in={isModifying} timeout="auto">
        <ModifyRule
          name={name}
          srcIp={srcIp}
          destIp={destIp}
          srcPort={srcPort}
          destPort={destPort}
          action={action}
          isInbound={isInbound}
        />
      </Collapse>
    </>
  );
}
