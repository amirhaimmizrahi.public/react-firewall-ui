import { Button, Collapse } from "@mui/material";
import React, { useState } from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import CreateNewRule from "./CreateNewRule";

export default function NewRuleCollapsible({isInbound}) {
  const [isCollapsed, setIsCollapsed] = useState(false);

  return (
    <>
      <Button
        variant="contained"
        sx={{ textTransform: "none", margin: "5px" }}
        onClick={() => {
          setIsCollapsed(!isCollapsed);
        }}
      >
        New Rules
        {isCollapsed ? <ExpandMoreIcon /> : <ExpandLessIcon />}
      </Button>
      <Collapse in={isCollapsed} timeout="auto">
        <CreateNewRule isInbound={isInbound}/>
      </Collapse>
    </>
  );
}
