import {
  Alert,
  Button,
  Dialog,
  FormControl,
  Grid,
  Paper,
  TextField,
} from "@mui/material";
import React, { useRef, useState } from "react";
import { useAddRule } from "../../api/handlers";

export default function CreateNewRule({ isInbound }) {
  const ruleNameRef = useRef();
  const ruleSrcIpRef = useRef();
  const ruleDestIpRef = useRef();
  const ruleSrcPortRef = useRef();
  const ruleDestPortRef = useRef();
  const ruleDesciptionRef = useRef();
  const addRule = useAddRule();
  const [addError, setAddError] = useState("");

  return (
    <Grid direction="column" container justifyContent="center" alignItems="center">
      <Grid sx={{ width: "30%", margin: "5px"}}>
        {addError ? (
          <Alert severity="error">
            {addError}
          </Alert>
        ) : null}
      </Grid>
      <Paper
        elevation={10}
        sx={{ padding: "20px", margin: "5px", width: "30%" }}
      >
        <Grid
          container
          width="100%"
          justifyContent="center"
          alignItems="center"
        >
          <Grid container>
            <Grid item xs={6}>
              <TextField
                inputRef={ruleNameRef}
                label="Rule Name"
                placeholder="Enter the rule name"
                sx={{ margin: "10px" }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                inputRef={ruleDesciptionRef}
                label="Rule action"
                placeholder="Enter the rule action"
                sx={{ margin: "10px" }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                inputRef={ruleSrcIpRef}
                label="Source IP"
                placeholder="Enter the source ip"
                sx={{ margin: "10px" }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                inputRef={ruleDestIpRef}
                label="Destination IP"
                placeholder="Enter the destination ip"
                sx={{ margin: "10px" }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                inputRef={ruleSrcPortRef}
                label="Source Port"
                placeholder="Enter the source port"
                sx={{ margin: "10px" }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                inputRef={ruleDestPortRef}
                label="Destination Port"
                placeholder="Enter the destination port"
                sx={{ margin: "10px" }}
              />
            </Grid>
          </Grid>
          <Button
            sx={{ margin: "10px" }}
            onClick={async () => {
              const error = await addRule({
                name: ruleNameRef.current.value,
                srcIp: ruleSrcIpRef.current.value,
                destIp: ruleDestIpRef.current.value,
                srcPort: ruleSrcPortRef.current.value,
                destPort: ruleDestPortRef.current.value,
                action: ruleDesciptionRef.current.value,
                isInbound: isInbound,
              });
              setAddError(error);
            }}
          >
            add new rule
          </Button>
        </Grid>
      </Paper>
    </Grid>
  );
}
