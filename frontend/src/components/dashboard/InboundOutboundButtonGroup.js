import React, { useState } from "react";
import CallReceivedIcon from "@mui/icons-material/CallReceived";
import CallMadeIcon from "@mui/icons-material/CallMade";
import { ToggleButton, ToggleButtonGroup } from "@mui/material";

export default function InboundOutboundButtonGroup({
  isInbound,
  toggleIsInbound,
}) {
  return (
    <ToggleButtonGroup
      value={isInbound ? "inbound" : "outbound"}
      onChange={toggleIsInbound}
      sx={{ margin: "5px" }}
      exclusive
    >
      <ToggleButton value="inbound">
        inbound
        <CallReceivedIcon />
      </ToggleButton>
      <ToggleButton value="outbound">
        outbound
        <CallMadeIcon />
      </ToggleButton>
    </ToggleButtonGroup>
  );
}
