import React, { useRef, useState } from "react";
import {
  Grid,
  Paper,
  TextField,
  Typography,
  Button,
  FormControl,
  Link,
  Alert,
} from "@mui/material";
import { useLogin } from "../api/handlers";

const paperStyle = {
  width: 400,
  height: 500,
};
const textFieldStyle = {
  margin: "5px 0",
};
const submitBtnStyle = {
  margin: "5px 0",
};
const formStyle = {
  margin: "50px 0",
};

export default function LoginPage() {
  const usernameRef = useRef();
  const passwordRef = useRef();
  const login = useLogin();
  const [loginError, setLoginError] = useState("");

  return (
    <Grid container alignItems="center" justifyContent="center">
      {loginError ? (
        <Alert
          severity="error"
          sx={{
            position: "absolute",
            transform: "translate(0, -50%)",
            top: "12.5%",
          }}
        >
          {loginError}
        </Alert>
      ) : null}

      <Paper style={paperStyle} elevation={10}>
        <Grid
          container
          height="100%"
          direction="column"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h2" component="h1">
            Login
          </Typography>

          <FormControl sx={formStyle}>
            <TextField
              inputRef={usernameRef}
              sx={textFieldStyle}
              label="username"
              placeholder="Enter username"
              variant="outlined"
              fullWidth
            />
            <TextField
              inputRef={passwordRef}
              sx={textFieldStyle}
              label="password"
              placeholder="Enter password"
              variant="outlined"
              fullWidth
            />
            <Button
              sx={submitBtnStyle}
              type="submit"
              color="primary"
              variant="contained"
              fullWidth
              onClick={async () => {
                const error = await login(
                  usernameRef.current.value,
                  passwordRef.current.value
                );
                setLoginError(error);
              }}
            >
              Sign in
            </Button>
          </FormControl>

          <Typography>
            Don't have and account yet?
            <Link href="../signup"> Sign up</Link>
          </Typography>
        </Grid>
      </Paper>
    </Grid>
  );
}
