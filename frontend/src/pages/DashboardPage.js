import { Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import DashboardRule from "../components/dashboard/DashboardRule";
import LogoutButton from "../components/dashboard/LogoutButton";
import InboundOutboundButtonGroup from "../components/dashboard/InboundOutboundButtonGroup";
import NewRuleCollapsible from "../components/dashboard/NewRuleCollapsible";
import RulesHeader from "../components/dashboard/RulesHeader";
import { useSelector } from "react-redux";
import { useFetch } from "../api/handlers";

export default function DashboardPage() {
  const rules = useSelector((state) => state.rules);
  const [isInbound, setIsInbound] = useState(true);
  const [modifiedRule, setModifiedRule] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const fetch = useFetch();

  function toggleIsInbound() {
    setIsInbound(!isInbound);
    setModifiedRule("");
  }

  useEffect(() => {
    fetch().then(setIsLoading(false));
  }, []);

  return (
    <Grid container direction="column" alignItems="center" margin="50px 0">
      <LogoutButton />
      <InboundOutboundButtonGroup
        isInbound={isInbound}
        toggleIsInbound={toggleIsInbound}
      />

      {isLoading ? (
        <Typography variant="h4" sx={{ margin: "20px" }}>
          Fetching Rules...
        </Typography>
      ) : isInbound ? (
        <>
          {rules.inbound.length > 0 ? <RulesHeader /> : null}
          {rules.inbound.map((rule) => (
            <DashboardRule
              name={rule.name}
              srcIp={rule.srcIp}
              destIp={rule.destIp}
              srcPort={rule.srcPort}
              destPort={rule.destPort}
              action={rule.action}
              isInbound={true}
              setModifiedRule={setModifiedRule}
              isModifying={modifiedRule === rule.name}
            />
          ))}
        </>
      ) : (
        rules.outbound.map((rule) => (
          <DashboardRule
            name={rule.name}
            srcIp={rule.srcIp}
            destIp={rule.destIp}
            srcPort={rule.srcPort}
            destPort={rule.destPort}
            action={rule.action}
            isInbound={false}
            setModifiedRule={setModifiedRule}
            isModifying={modifiedRule === rule.name}
          />
        ))
      )}

      <NewRuleCollapsible isInbound={isInbound} />
    </Grid>
  );
}
