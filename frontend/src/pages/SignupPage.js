import React, { useRef, useState } from "react";
import {
  Grid,
  Paper,
  TextField,
  Typography,
  Button,
  FormControl,
  Link,
  Alert,
} from "@mui/material";
import { useSignup } from "../api/handlers";

const paperStyle = {
  width: 400,
  height: 500,
};
const textFieldStyle = {
  margin: "5px 0",
};
const submitBtnStyle = {
  margin: "5px 0",
};
const formStyle = {
  margin: "50px 0",
};

export default function SignupPage() {
  const usernameRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const [signupError, setSignupError] = useState("");
  const signup = useSignup();

  return (
    <Grid container alignItems="center" justifyContent="center">
      {signupError ? (
        <Alert
          severity="error"
          sx={{
            position: "absolute",
            transform: "translate(0, -50%)",
            top: "12.5%",
          }}
        >
          {signupError}
        </Alert>
      ) : null}

      <Paper style={paperStyle} elevation={10}>
        <Grid
          container
          height="100%"
          direction="column"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h2" component="h1">
            Signup
          </Typography>

          <FormControl sx={formStyle}>
            <TextField
              sx={textFieldStyle}
              label="username"
              placeholder="Enter username"
              variant="outlined"
              fullWidth
              inputRef={usernameRef}
            />
            <TextField
              sx={textFieldStyle}
              label="password"
              placeholder="Enter password"
              variant="outlined"
              fullWidth
              inputRef={passwordRef}
            />
            <TextField
              sx={textFieldStyle}
              label="confirm password"
              placeholder="Confirm password"
              variant="outlined"
              fullWidth
              inputRef={passwordConfirmRef}
            />
            <Button
              sx={submitBtnStyle}
              type="submit"
              color="primary"
              variant="contained"
              fullWidth
              onClick={async () => {
                const error = await signup(
                  usernameRef.current.value,
                  passwordRef.current.value,
                  passwordConfirmRef.current.value
                );
                setSignupError(error);
              }}
            >
              Sign up
            </Button>
          </FormControl>

          <Typography>
            Already have an account?
            <Link href="../login"> Sign in</Link>
          </Typography>
        </Grid>
      </Paper>
    </Grid>
  );
}
