import axios from "axios";

const HOST = "http://localhost:9001/";

export function loginRequest(username, password) {
  return axios.post(HOST + "login", {
    username,
    password,
  });
}

export function signupRequest(username, password, passwordConfirm) {
  return axios.post(HOST + "signup", {
    username,
    password,
    passwordConfirm,
  });
}

export function fetchRequest(user) {
  return axios.post(HOST + "fetch", {
    ...user,
  });
}

export function addRuleRequest(user, newRule) {
  return axios.post(HOST + "add", {
    ...user,
    rule: newRule,
  });
}

export function removeRequest(user, ruleToDelete, isInbound) {
  return axios.post(HOST + "remove", {
    ...user,
    ruleName: ruleToDelete,
    isInbound,
  });
}

export function modifyRequest(user, ruleToModify, newRule) {
  return axios.post(HOST + "modify", {
    ...user,
    ruleName: ruleToModify,
    rule: newRule,
  });
}
