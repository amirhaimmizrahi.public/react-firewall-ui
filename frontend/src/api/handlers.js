import { login } from "../dl/slices/user";
import {
  addRuleRequest,
  fetchRequest,
  loginRequest,
  modifyRequest,
  removeRequest,
  signupRequest,
} from "./requests";
import { store } from "../dl/store";
import { useNavigate } from "react-router-dom";
import { addRule, clear, modifyRule, remove } from "../dl/slices/rules";

export function useLogin() {
  const navigate = useNavigate();

  return async (username, password) => {
    const res = await loginRequest(username, password);
    if (res.data == "ok") {
      store.dispatch(login([username, password]));
      navigate("../dashboard");

      return "";
    } else {
      return res.data;
    }
  };
}

export function useSignup() {
  const navigate = useNavigate();

  return async (username, password, passwordConfirm) => {
    const res = await signupRequest(username, password, passwordConfirm);
    
    if (res.data == "ok") {
      navigate("../login");
      return "";
    } else {
      return res.data;
    }
  };
}

export function useFetch() {
  return async () => {
    const user = store.getState().user;
    const res = await fetchRequest(user);

    if (res.data) {
      const rules = res.data;
      store.dispatch(clear());

      rules.inbound.forEach((rule) => {
        store.dispatch(addRule({ ...rule, isInbound: true }));
      });
      rules.outbound.forEach((rule) => {
        store.dispatch(addRule({ ...rule, isInbound: false }));
      });
    }
  };
}

export function useAddRule() {
  return async (newRule) => {
    const user = store.getState().user;
    const res = await addRuleRequest(user, newRule);

    if (res.data == "ok") {
      store.dispatch(addRule(newRule));
      return "";
    } else {
      return res.data;
    }
  };
}

export function useRemove() {
  return async (ruleToRemove, isInbound) => {
    const user = store.getState().user;
    const res = await removeRequest(user, ruleToRemove, isInbound);

    if (res.data === "ok") {
      store.dispatch(remove({ isInbound, ruleName: ruleToRemove }));
      return "";
    } else {
      return res.data;
    }
  };
}

export function useModify() {
  return async (ruleToModify, newName, newAction, newSrcIp, newDestIp, newSrcPort, newDestPort, isInbound) => {
    const user = store.getState().user;
    const newRule = {
      name: newName,
      action: newAction,
      srcIp: newSrcIp,
      destIp: newDestIp,
      srcPort: newSrcPort,
      destPort: newDestPort,
      isInbound,
    };
    const res = await modifyRequest(user, ruleToModify, newRule);

    if (res.data == "ok") {
      store.dispatch(modifyRule({ rule: newRule, ruleName: ruleToModify }));
      return "";
    } else {
      return res.data;
    }
  };
}
