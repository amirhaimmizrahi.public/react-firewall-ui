import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  inbound: [],
  outbound: [],
};

export const rulesSlice = createSlice({
  name: "rules",
  initialState,
  reducers: {
    addRule(state, action) {
      if (action.payload.isInbound) {
        state.inbound.push({
          name: action.payload.name,
          srcIp: action.payload.srcIp,
          destIp: action.payload.destIp,
          srcPort: action.payload.srcPort,
          destPort: action.payload.destPort,
          action: action.payload.action,
        });
      } else {
        state.outbound.push({
          name: action.payload.name,
          srcIp: action.payload.srcIp,
          destIp: action.payload.destIp,
          srcPort: action.payload.srcPort,
          destPort: action.payload.destPort,
          action: action.payload.action,
        });
      }
    },
    modifyRule(state, action) {
      if (action.payload.rule.isInbound) {
        const ruleToModify = state.inbound.find(
          (current) => current.name === action.payload.ruleName
        );

        const index = state.inbound.indexOf(ruleToModify);
        state.inbound[index] = {name: action.payload.rule.name, action: action.payload.rule.action, srcIp: action.payload.rule.srcIp, destIp: action.payload.rule.destIp, srcPort: action.payload.rule.srcPort, destPort: action.payload.rule.destPort, isInbound: action.payload.rule.isInbound};
      } else {
        const ruleToModify = state.outbound.find(
          (current) => current.name === action.payload.ruleName
        );

        const index = state.outbound.indexOf(ruleToModify);
        state.outbound[index] = {name: action.payload.rule.name, action: action.payload.rule.action, srcIp: action.payload.rule.srcIp, destIp: action.payload.rule.destIp, srcPort: action.payload.rule.srcPort, destPort: action.payload.rule.destPort, isInbound: action.payload.rule.isInbound};
      }
    },
    clear(state, action) {
      state.inbound = [];
      state.outbound = [];
    },
    remove(state, action) {
      if (action.payload.isInbound) {
        const ruleToRemove = state.inbound.find(
          (rule) => rule.name === action.payload.ruleName
        );
        const index = state.inbound.indexOf(ruleToRemove);
        state.inbound.splice(index, 1);
      } else {
        const ruleToRemove = state.outbound.find(
          (rule) => rule.name === action.payload.ruleName
        );
        const index = state.outbound.indexOf(ruleToRemove);
        state.outbound.splice(index, 1);
      }
    },
  },
});

// Action creators are generated for each case reducer function
export const { addRule, clear, remove, modifyRule } = rulesSlice.actions;

export default rulesSlice.reducer;
