import { configureStore } from "@reduxjs/toolkit";
import rulesReducer from "./slices/rules";
import userReducer from "./slices/user";

export const store = configureStore({
  reducer: {
    rules: rulesReducer,
    user: userReducer,
  },
});

window.store = store;
