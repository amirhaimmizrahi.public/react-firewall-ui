## Table of Content
---
- [Table of Content](#table-of-content)
- [Architechture](#architechture)
- [Frontend](#frontend)
  - [Redux](#redux)
  - [API](#api)
  - [React](#react)
- [Backend](#backend)
  - [MongoDB](#mongodb)

## Architechture
---
<img src="./architecture.png"  width="110%" height="110%">

## Frontend
---
The frontend consists of three parts:
 - [Redux](#redux)
 - [API](#api)
 - [React](#react) 
  
### Redux
Redux is used to store and access frequent used data without the need to pass the data along the component tree.

The store is made from two slices:
- **rules slice**: store inbound and outbound rules.
- **user slice**: store info about the user - username, password, tokens...

The rules slice contains the following reducers:
 - `addRule`
 - `modifyRule`
 - `clear` to remove all stored rules
 - `remove` to remove an existing rule 

The user slice contains a single reducer used to `login`.
If I used login tokens and cookies, I would create another reducer to logout.
  
### API
The api part consists of two modules: `requests` and `handlers`.

The `requests` module makes axios post requests to the backend.
Axios library is used for easy and clean api calls.

The `handlers` module consists of multiple react hooks that create function to make api calls and handle them.

The react hooks are:
- **useLogin**: Returns a function that makes a *login requests* to the backend. This function returns either an error message or an empty string.
- **useSignup**: Returns a function that makes a *signup request* to the backend. This function returns either an error message or an empty string.
- **useFetch**: Returns a function that *fetches* all the existing rules of the user.
- **useAddRule**: Returns a function that *adds a new rule*. This function returns either an error message or an empty string.
- **useRemove**: Returns a function that *removes an existing rule*. This function returns either an error message or an empty string.
- **useModify**: Returns a function that *modifies an existing rule*. This function returns either an error message or an empty string.

### React
The ui is made with Material UI for easier and more consistent components look.

The app consists of three pages:
 - Login page
 - Signup page
 - Dashboard page
  
The **login page** takes the username and password from the user and make a login request.

The **signup page** takes information from the user and make a signup request.

The **dashboard page** displays all the rules of the user and allows the user to create new rules, modify and remove existing ones.

The dashboard page contains multiple components to represent existing rules, form to create new rules, form to modify existing rules, button, switches and more.

## Backend
---
The backend listens for user requests and handle them.

Each handler first validate the user, then validate the inputs i.e. the username and password of a login request and then handle the request.

The handlers send back to the user either an error message or `"ok"`.

### MongoDB
The database is made with MongoDB.
The database has 2 collections:
 - users
 - rules

The users collection contains `User` schemas.
Each `User` schema represent a user and stores the user's username, password and arrays that reference the user's rules.

The rules collection contains `Rule` schemas.
Each `Rule` schema represent a rule and stores information about a rule, i.e. rule name, source ip, destination port etc.

I chose to use MongoDB because I've never used it I thought its a good time to learn and practice it.