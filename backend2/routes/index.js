var express = require("express");
var router = express.Router();

const users = {
  test: {
    password: "test",
    rules: {
      inbound: [
        { name: "First Rule", action: "Deny UDP packets" },
        { name: "Second Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Third Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Fourth Rule", action: "Deny UDP packets" },
        { name: "Fifth Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Sixth Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Seventh Rule", action: "Deny UDP packets" },
        { name: "Eighth Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Ninth Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Tenth Rule", action: "Deny UDP packets" },
        { name: "Eleventh Rule", action: "Deny packets from 1.1.1.1" },
        { name: "Twelth Rule", action: "Deny packets from 1.1.1.1" },
      ],
      outbound: [
        { name: "Outgoing First Rule", action: "Deny UDP packets" },
        {
          name: "Outgoing Second Rule",
          action: "Deny packets from 1.1.1.1",
        },
      ],
    },
  },
  empty: {
    password: "empty",
    rules: {
      inbound: [],
      outbound: [],
    },
  },
};

function userValidation(req, res) {
  const { username, password } = req.body;
  if (!(username && password)) {
    res.send("please provide username and password");
    return false;
  }
  if (!users[username] || users[username].password != password) {
    res.send("please make sure that your username and password are corrent");
    return false;
  }
  return true;
}

//handle login
router.post("/login", function (req, res, next) {
  if (userValidation(req, res)) {
    res.send("ok");
  }
});

router.post("/signup", function (req, res, next) {
  const { username, password, passwordConfirm } = req.body;

  if (!(username && password && passwordConfirm)) {
    res.send("please provide username, password and password confirm");
    return;
  }
  if (password != passwordConfirm) {
    res.send("password does not match the password confirm");
    return;
  }
  if (users[username]) {
    res.send("username is taken");
    return;
  }
  users[username] = {
    password: password,
    rules: {
      inbound: [],
      outbound: [],
    },
  };
  res.send("ok");
});

router.post("/fetch", function (req, res, next) {
  if (!userValidation(req, res)) {
    return;
  }
  const { username } = req.body;
  res.json(users[username].rules);
});

router.post("/add", function (req, res, next) {
  if (!userValidation(req, res)) {
    return;
  }
  const { username, rule } = req.body;

  if (!(rule.name && rule.action && rule.isInbound !== undefined)) {
    res.send("please specify a rule to add");
    return;
  }

  if (rule.isInbound) {
    if (
      users[username].rules.inbound.find(
        (current) => current.name === rule.name
      )
    ) {
      res.send("rule with the same name is already exists");
      return;
    }

    users[username].rules.inbound.push({
      name: rule.name,
      action: rule.action,
    });
  } else {
    if (
      users[username].rules.outbound.find(
        (current) => current.name === rule.name
      )
    ) {
      res.send("rule with the same name is already exists");
      return;
    }

    users[username].rules.outbound.push({
      name: rule.name,
      action: rule.action,
    });
  }
  res.send("ok");
});

router.post("/modify", function (req, res, next) {
  if (!userValidation(req, res)) {
    return;
  }
  const { username, ruleName, rule } = req.body;
  if (!(rule.name && rule.action && rule.isInbound !== undefined)) {
    res.send("please specify a the new rule name and action");
    return;
  }

  if (rule.isInbound) {
    const ruleToModify = users[username].rules.inbound.find(
      (current) => current.name === ruleName
    );

    if (
      users[username].rules.inbound.find(
        (current) => current.name === rule.name && current.name !== ruleName
      )
    ) {
      res.send("rule with the same name is already exists");
      return;
    }

    if (ruleToModify === undefined) {
      res.send("rule was not found");
      return;
    }

    const index = users[username].rules.inbound.indexOf(ruleToModify);
    users[username].rules.inbound[index] = {
      name: rule.name,
      action: rule.action,
    };
  } else {
    const ruleToModify = users[username].rules.outbound.find(
      (current) => current.name === ruleName
    );

    if (
      users[username].rules.outbound.find(
        (current) => current.name === rule.name && current.name !== ruleName
      )
    ) {
      res.send("rule with the same name is already exists");
      return;
    }

    if (ruleToModify === undefined) {
      res.send("rule was not found");
      return;
    }

    const index = users[username].rules.outbound.indexOf(ruleToModify);
    users[username].rules.outbound[index] = {
      name: rule.name,
      action: rule.action,
    };
  }
  res.send("ok");
});

router.post("/remove", function (req, res, next) {
  if (!userValidation(req, res)) {
    return;
  }
  const { username, ruleName, isInbound } = req.body;

  if (!ruleName || isInbound === undefined) {
    res.send("please specify the rule to remove");
    return;
  }

  if (isInbound) {
    const ruleToRemove = users[username].rules.inbound.find(
      (rule) => rule.name === ruleName
    );
    const index = users[username].rules.inbound.indexOf(ruleToRemove);
    users[username].rules.inbound.splice(index, 1);
  } else {
    const ruleToRemove = users[username].rules.outbound.find(
      (rule) => rule.name === ruleName
    );
    const index = users[username].rules.outbound.indexOf(ruleToRemove);
    users[username].rules.outbound.splice(index, 1);
  }
  res.send("ok");
});

module.exports = router;
