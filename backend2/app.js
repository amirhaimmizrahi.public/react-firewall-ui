var express = require("express");
var cors = require("cors");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const mongoose = require("mongoose");

var indexRouter = require("./routes/index");

mongoose
  .connect("mongodb://localhost:27017/firewall", {
    useNewUrlPraser: true,
    useUnifiedTopology: true,
  })
  .then((result) => console.log("database connected"))
  .catch((err) => console.log(err));

app.listen(3002, () => {
  console.log("database starts on port 3002");
});

var app = express();
app.use(cors());

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

module.exports = app;
